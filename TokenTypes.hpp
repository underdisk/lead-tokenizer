/*
 * @author underdisk
 */

#ifndef MODULES_TOKENTYPES_HPP
#define MODULES_TOKENTYPES_HPP

#include "TokenType.hpp"

namespace lead
{
    class StringTokenType : public ExclusiveTokenType
    {
    public:
        explicit StringTokenType(ExclusionZoneContext& context)
            : ExclusiveTokenType("([\"'])(?:(?=(\\\\?))\\2.)*?\\1", "STRING", context)
        {	}
    };

    class DecimalNumberTokenType : public ExclusiveTokenType
    {
    public:
        explicit DecimalNumberTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("([^\\w]|[\\{\\}\\[\\]])[0-9]+[.]?[0-9]*([eE][-+]?[0-9]+)?", "DECNUM", context)
        {	}
    };

    class IdentifierTokenType : public ExclusiveTokenType
    {
    public:
        explicit IdentifierTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\w+", "IDENTI", context)
        {	}
    };

    class OperatorTokenType : public ExclusiveTokenType
    {
    public:
        explicit OperatorTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("[\\*\\+\\-\\/\\=]", "OPERAT", context)
        {	}
    };

    class OpenBraceTokenType : public ExclusiveTokenType
    {
    public:
        explicit OpenBraceTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\{", "OPENCB", context)
        {	}
    };

    class ClosedBraceTokenType : public ExclusiveTokenType
    {
    public:
        explicit ClosedBraceTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\}", "CLOSCB", context)
        {	}
    };

    class OpenParenthesisTokenType : public ExclusiveTokenType
    {
    public:
        explicit OpenParenthesisTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\(", "OPENPA", context)
        {	}
    };

    class ClosedParenthesisTokenType : public ExclusiveTokenType
    {
    public:
        explicit ClosedParenthesisTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\)", "CLOSPA", context)
        {	}
    };

    class OpenSquareBracketTokenType : public ExclusiveTokenType
    {
    public:
        explicit OpenSquareBracketTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\[", "OPENSB", context)
        {	}
    };

    class ClosedSquareBracketTokenType : public ExclusiveTokenType
    {
    public:
        explicit ClosedSquareBracketTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\]", "CLOSSB", context)
        {	}
    };

    class SemicolonTokenType : public ExclusiveTokenType
    {
    public:
        explicit SemicolonTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\;", "SEMICO", context)
        {	}
    };

    class SimpleDotTokenType : public ExclusiveTokenType
    {
    public:
        explicit SimpleDotTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\.", "SIMDOT", context)
        {	}
    };

    class CommaTokenType : public ExclusiveTokenType
    {
    public:
        explicit CommaTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\,", "SCOMMA", context)
        {	}
    };

    class ReferenceCharacterTokenType : public ExclusiveTokenType
    {
    public:
        explicit ReferenceCharacterTokenType(ExclusionZoneContext& context)
                : ExclusiveTokenType("\\&", "REFCHR", context)
        {	}
    };
}

#endif //MODULES_TOKENTYPES_HPP
