/*
 * @author underdisk
 */

#include "Token.hpp"

namespace lead
{
    bool Token::operator<(const lead::Token &token) const
    {
        return (position < token.position);
    }
}

