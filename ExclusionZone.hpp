/*
 * @author underdisk
 */

#ifndef MODULES_EXCLUSIONZONE_HPP
#define MODULES_EXCLUSIONZONE_HPP

#include <vector>
#include <memory>
#include <unordered_map>
#include <string>

namespace lead {

class ExclusionZone
{
public:
    ExclusionZone() = default;

private:
    friend class ExclusionZoneContext;

    ExclusionZone(int start, int end)
        : start(start), end(end)
    {}

    unsigned int start, end;
};

class ExclusionZoneContext
{
public:
    ExclusionZoneContext()
        : m_zones()
    {}

    bool canBeToken(const std::string& literal, unsigned int position) const;

    void addExclusionZone(int start, int end);

private:
    std::vector<ExclusionZone> m_zones;
};

}

#endif //MODULES_EXCLUSIONZONE_HPP
