/*
 * @author underdisk
 */

#include "Tokenizer.hpp"
#include <iostream>

namespace lead
{
    Tokenizer::Tokenizer()
        : m_commentRegex("\\/\\/.*")
    {

    }

    const std::vector<Token>& Tokenizer::tokenize(std::string file)
    {
        ExclusionZoneContext context;

        StringTokenType stringTokenType(context);
        DecimalNumberTokenType decimalNumberTokenType(context);
        IdentifierTokenType identifierTokenType(context);
        OperatorTokenType operatorTokenType(context);
        OpenBraceTokenType openBraceTokenType(context);
        ClosedBraceTokenType closedBraceTokenType(context);
        OpenParenthesisTokenType openParenthesisTokenType(context);
        ClosedParenthesisTokenType closedParenthesisTokenType(context);
        OpenSquareBracketTokenType openSquareBracketTokenType(context);
        ClosedSquareBracketTokenType closedSquareBracketTokenType(context);
        SemicolonTokenType semicolonTokenType(context);
        SimpleDotTokenType simpleDotTokenType(context);
        CommaTokenType commaTokenType(context);
        ReferenceCharacterTokenType referenceCharacterTokenType(context);

        m_tokens = std::make_unique<std::vector<Token>>();
        std::vector<Token>& tokens = *m_tokens; //easy to use reference

        file = std::regex_replace(file, m_commentRegex, ""); //delete comments

        stringTokenType.createToken(file, tokens);
        decimalNumberTokenType.createToken(file, tokens);
        identifierTokenType.createToken(file, tokens);
        operatorTokenType.createToken(file, tokens);
        openBraceTokenType.createToken(file, tokens);
        closedBraceTokenType.createToken(file, tokens);
        openParenthesisTokenType.createToken(file, tokens);
        closedParenthesisTokenType.createToken(file, tokens);
        openSquareBracketTokenType.createToken(file, tokens);
        closedSquareBracketTokenType.createToken(file, tokens);
        semicolonTokenType.createToken(file, tokens);
        simpleDotTokenType.createToken(file, tokens);
        commaTokenType.createToken(file, tokens);
        referenceCharacterTokenType.createToken(file, tokens);

        //Check for reserved words (while, if, for, import, true, false, return...)
        for (auto& token : tokens)
		{
			if (token.type == "IDENTI")
			{
				if 		(token.literal == "while")
					token.type = "WHLOOP";
				else if (token.literal == "for")
					token.type = "FRLOOP";
				else if (token.literal == "if")
					token.type = "IFSTMT";
				else if (token.literal == "import")
					token.type = "IMPORT";
				else if (token.literal == "true" || token.literal == "false")
					token.type = "BOOLVL";
				else if (token.literal == "return")
					token.type = "RETURN";
				else if (token.literal == "mut")
					token.type = "MUTBLE";
				else if (token.literal == "void"
					  || token.literal == "i8" || token.literal == "ui8"
					  || token.literal == "i16" || token.literal == "ui16"
					  || token.literal == "i32" || token.literal == "ui32"
					  || token.literal == "i64" || token.literal == "ui64"
					  || token.literal == "float" || token.literal == "long"
					  || token.literal == "double")
					token.type = "PODTYP";
				else if (token.literal == "class" || token.literal == "struct")
					token.type = "STRUCT";
			}
			else if (token.type == "DECNUM")
			{
				token.literal = std::string(token.literal.c_str() + 1);
				token.position += 1;
			}
		}

        std::sort(tokens.begin(), tokens.end());

        return tokens;

    }

}