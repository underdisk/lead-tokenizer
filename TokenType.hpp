/*
 * @author underdisk
 */

#ifndef MODULES_TOKENTYPE_HPP
#define MODULES_TOKENTYPE_HPP

#include <string>
#include <memory>
#include <regex>

#include "Token.hpp"
#include "ExclusionZone.hpp"

namespace lead {
    class TokenType {
    public:
        TokenType(const std::string& regex, const std::string& name, ExclusionZoneContext& context);

        [[nodiscard]] const std::regex& getRegex() const;
        [[nodiscard]] virtual const std::string& getName() const;
        [[nodiscard]] ExclusionZoneContext& getContext() const;

        virtual void createToken(const std::string& file, std::vector<Token>& tokens);

    protected:

        void setRegex(const std::string& regex);

        std::unique_ptr<std::regex> m_regex;
        std::string m_name;
        ExclusionZoneContext& m_context;

    };

    class ExclusiveTokenType : public TokenType
    {
    public:
        ExclusiveTokenType(const std::string& regex, const std::string& name, ExclusionZoneContext& context);

        void createToken(const std::string& file, std::vector<Token>& tokens) override;
		[[nodiscard]] virtual const std::string& getName() const override;

    protected:

    private:
    };
}

#endif //MODULES_TOKENTYPE_HPP
