/*
 * @author underdisk
 */

#ifndef MODULES_TOKENIZER_HPP
#define MODULES_TOKENIZER_HPP

#include <regex>
#include <string>
#include <vector>
#include <memory>
#include <functional>
#include <algorithm>
#include <unordered_map>

#include "TokenTypes.hpp"

namespace lead
{

    class Tokenizer
    {
    public:
        Tokenizer();

        const std::vector<Token>& tokenize(std::string file);

    protected:

    private:

        std::unique_ptr<std::vector<Token>> m_tokens;
        std::regex m_commentRegex;

        ExclusionZoneContext m_context;

    };

}



#endif //MODULES_TOKENIZER_HPP
