/*
 * @author underdisk
 */

#ifndef MODULES_TOKEN_HPP
#define MODULES_TOKEN_HPP

#include <string>

namespace lead
{
    struct Token
    {
        Token(int position, const std::string& literal, const std::string& type)
                : position(position), literal(literal), type(type)
        {}

        bool operator<(const Token& token) const;

        int position;
        std::string literal;
        std::string type;
    };
}




#endif //MODULES_TOKEN_HPP
